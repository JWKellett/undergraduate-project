/*  Measures the value of all the sensors in the prototype and prints the data to the serial monitorl.
 *  
 *  There are 4 agents in the test configuration that are referred to as follows:
 *   *   __________    ___________   ____________
 *    | |*LT     RT*| |*LT        | |           |
 *    | |  \     /  | |  \     /  | |  \     /  |
 *    | |  Agent A  | |  Agent B  | |  Agent C  |
 *    | |  /     \  | |  /     \  | |  /     \  |
 *   _| |*LB_____RB*| |*LB________| |___________|
 *   _   ___________
 *    | |*LT        |
 *    | |  \     /  |
 *    | |  Agent D  |
 *    | |  /     \  |
 *   _| |*LB________|
 *  
 */

// Pin definitions
const int pinALT = 15;
const int pinALB = 14;
const int pinLedAL = 2;

const int pinART = 12;
const int pinARB = 11;
const int pinLedAR = 3;


const int pinBLT = 9;
const int pinBLB = 8;
const int pinLedBL = 4;


const int pinDLT = 7;
const int pinDLB = 6;
const int pinLedDL = 5;


const int no_readings = 10;   // Number of readings to average over
const int minReading = 300;   // Sensor value below which to read 0


void process(String location, int pinTop, int pinBottom, int pinLED, int moment_limit);

void setup(void) {
  Serial.begin(9600);         // Initialise serial monitor
  pinMode(pinLedAL, OUTPUT);
  pinMode(pinLedAR, OUTPUT);
  pinMode(pinLedBL, OUTPUT);
  pinMode(pinLedDL, OUTPUT);
}
 
void loop(void) {
  // Process readings (different moment threshold for each sensor pair due to differences in manufacture)
  process("A_Left", pinALT, pinALB, pinLedAL, 90);
  process("A_right", pinART, pinARB, pinLedAR, -80);
  process("B_Left", pinBLT, pinBLB, pinLedBL, -80);
  process("D_Left", pinDLT, pinDLB, pinLedDL, -800);


  // End line and pause for sampling frequency of 100 Hz
  Serial.println(" ");
  delay(10);
}

void process(String location, int pinTop, int pinBottom, int pinLED, int moment_limit) {
  int sumTop = 0;
  int sumBottom = 0;
  
  // Read and average values
  for (int i = 0; i < no_readings; i++){
    sumTop += analogRead(pinTop);         // Read pin of top sensor
    sumBottom += analogRead(pinBottom);         // Read pin of bottom sensor
    delay(5);
  }
  
  // Calculate average reading
  int averageTop = sumTop / no_readings;
  int averageBottom = sumBottom/ no_readings;

  // Convert to a moment
  int moment = averageBottom - averageTop;

  // If high moment, turn on warning LED
  if (moment < moment_limit || averageTop==1023) {
    digitalWrite(pinLED, HIGH);
  }
  else {
    digitalWrite(pinLED, LOW);
  }

  // Write value to serial port
  Serial.print(location);
  Serial.print("_moment:");
  Serial.print(moment);
  Serial.print(" ");
  
  }
