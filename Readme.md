# Distributed Self-Assembly of Cantilevers by Force-Aware Robots 

This repository contains the source code for constructing cantilevers from self-assembling force-aware robots, published as:  E. Bray and R. Gross, "*Distributed Self-Assembly of Cantilevers by Force-Aware Robots*", IEEE International Symposium on Multi-Robot and Multi-Agent Systems, 2021.

In the paper, each robot is referred to as an "agent", however note that they are referred to as "elements" in the source code.

## Installation

The repository has two branches: `main` contains only the code, and `examples` also contains example results. When cloning, choose which branch you would like with the `-b` flag; the `main` branch will be faster to download.

Code was developed in Python 3.8.0, and is only tested for this version. Due to package dependency conflicts, it is recommended to create a clean virtual environment, then install the required modules with:

```bash
pip install -r requirements.txt --no-dependencies
```

By default, results are saved to `output`. This can be changed by modifying the `data_dir` function on line 13 of `package/basics.py`.



## Sequential algorithm

A single trial of the sequential algorithm can be called with `cantilever_algorithm_sequential.py`. By default, a 30 agent trial of the local variant with weak links is run. See additional options with the `--help` flag. For example, a 40 agent trial of the local variant with links of medium strength that creates a video of the results can be run with:

```bash
python cantilever_algorithm_sequential.py 40 426 1159 -s
```

If required, results are saved to `output/auto-generated_structures/[trial_parameters]/[Trial_x.csv]`. The sequential algorithm creates a `.csv` file in which the first column is the number of agents in the structure, and the second is the cantilever configuration as a string containing the number of agents in each row. A video is also created. The motion of agents as they travel is not saved or plotted.



## Parallel algorithm

A single trial of the parallel algorithm can be called with `cantilever_algorithm_parallel.py`. By default, a 30 agent trial with weak links and a delay of 8 steps per agent is run. See additional options with the `--help` flag. For example, a 40 agent trial with links of medium strength and an insertion delay of 10 steps, that creates a video of the results can be run with:

```bash
python cantilever_algorithm_parallel.py 40 426 1159 10 -s
```

If required, results are saved to `output/auto-generated_structures/[trial_parameters]/[Trial_x.pbz2]`. The parallel algorithm creates a Pickled file, compressed them with bz2 compression, which contains the state of the cantilever at every step. A video is also created, which shows the motion of agents as they travel.



## Replotting existing cantilevers

Structures can be replotted using `draw_existing_cantilever.py`. Specify the filename, step number, and allowable limits at the top of this file. This can plot either `.csv` files from the sequential algorithm, or `.pbz2` files from the parallel algorithm.



## Optimisation

Optimal structures are calculated using `cantilever_structural_optimisation.py`. There are six modes, as detailed at the top of this file. Data are saved to `output/Optimisation`. The workflow for generating optimal structures is as follows:

1. Test all possible configurations of $n$​ elements with the `enumerate` mode. This is computationally expensive, but the precomputed data can quickly be analysed for any link strength. It's a good idea to enumerate in this way for as large an $n$ as your computer will tractably allow you to.
2. Analyse these precomputed data with the `optimums` mode. Results are saved to a directory for each limit pair.
3. If you would like larger optimal structures, the process is slower. Use the `enumerate_large` mode to enumerate structures 1 agent longer than the longest known structure for the specified limit pair, and with a specified number of agents. 
4. Check if the enumerated configurations in the previous step are stable or not with the `optimums_large` mode.

Repeat steps 3 and 4 to find the smallest $n$​​ that allows structures of the given length. For example, if you find weak links can build structures 15 agents long with 63 agents, try with fewer agents until you find the smallest possible.

If you find you have calculated all the optimal structures you can, then add another file called `XXelements_YYl_UNSTABLE.csv` where XX is the largest number of agents you've tried to make a structure YY agents long and been unsuccessful. This will pad the results files to this given length.

You can collate the data to `elements_to_length.csv` and `length_to_elements.csv` files, as well as draw a graph of the results, with the `collate` mode.

Finally, the `draw` mode allows you to draw optimal configurations.



## Multiple trials

Since the algorithms are stochastic, multiple trials must be run to see the average performance. This is done with the `cantilever_multiple_trials.py` script. There are six modes, as detailed at the top of this file. Data are saved to `output/Results`. The workflow for running these trials is as follows:

1. The trials are run with the `run` mode. You can specify the number of trials, how many agents per trial, the link strength, and (for the parallel algorithm) the delay between adding new agents.  Results are  saved in a directory for the algorithm name, containing another one for each limit pair, and (in the parallel algorithm) another directory for the delay between adding new agents. There is a `.csv` file containing condensed information about the trial, and a `.out` file which contains what would have been printed to the terminal for inspection. In the parallel algorithm, we also save a `.pbz2` file for the first five trials to enable replotting.
2. (a) If running the sequential algorithm, you should generate optimal structures for this link strength too before continuing (see above).
   (b) If running the parallel algorithm, you will need to also use the `run` mode with the algorithm set to `parallel_delay0` and the delay set to 0 in order to draw comparisons to the sequential algorithm. This runs the sequential algorithm in the parallel environment, so the results are more comparable. You should then use the `process_parallel_delay0` mode to analyse these data for comparison before continuing.
3. Once the trials have finished, you must first process the data for each limit pair separately with the `process_limit_pair` mode. This produces graphs in the limit pair directory.
4. The above graphs can be collated into a single plot for easier comparison with the `collate` mode.
5. Compare different versions of the sequential algorithm with the `compare_sequential` mode, and different versions of the parallel algorithm with the `compare_parallel` mode. These comparisons are saved to the `output/Results/comparisons` directory.

