"""compare_parallel mode"""
import os

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

from ...basics import collated_label, data_dir, make_dir, show_or_save
from ...graphics.colours import colours_iterator, custom_sns_palette
from .process_parallel import (delays, element_delay_label,
                               generate_barplot_df, read_preprocessed_data)
from .shared import limit_directory, limits_in_directory


def compare_parallel_versions(algorithm_names, plots):
    """Compares the different versions of the parallel algorithm"""

    # Make output directories if necessary
    output_dir = os.path.join(data_dir(),
                              'Results',
                              'comparisons',
                              'parallel-' + '-'.join(algorithm_names))

    if plots == 'save':
        make_dir(output_dir)

    # Import data
    link_strengths, element_delays = define_figure_size(algorithm_names)
    imported_data, barplot_df = import_data(algorithm_names)

    # Plot length_steps
    length_steps_fig = plot_length_steps(imported_data, algorithm_names, link_strengths,
                                         element_delays)
    show_or_save(length_steps_fig, plots, os.path.join(output_dir, 'length_steps'))

    # Plot barplots showing consistency of behaviour
    for col_title, filename in barplot_titles():
        barplot_fig = plot_barplots(barplot_df, col_title, link_strengths)
        show_or_save(barplot_fig, plots, os.path.join(output_dir, filename))


def algorithm_filename(algorithm_name):
    """Converts the algorithm name to a filename"""

    if algorithm_name == 'standard':
        return 'parallel'

    return 'parallel_{}'.format(algorithm_name)


def import_data(algorithm_names):
    """Imports the data from the specified trials into a dictionary"""

    imported_data = {}
    barplot_dfs = []

    for algorithm_name in algorithm_names:
        print('Loading parallel_'+algorithm_name)

        imported_data[algorithm_name] = {}

        for limits in limits_in_directory(algorithm_filename(algorithm_name)):
            limit_str = '{}m_{}a'.format(limits['moment'],
                                         limits['axial'])

            print('  Loading for {}'.format(limit_str))

            # Load dataframes for this limit pair into a list
            limit_dir = limit_directory(algorithm_filename(algorithm_name), limits)

            (length_steps_dfs, trials_collated_dfs, _) = read_preprocessed_data(limit_dir)

            imported_data[algorithm_name][limit_str] = {'length_steps': length_steps_dfs,
                                                        'trials_collated': trials_collated_dfs,
                                                        'delays': delays(limit_dir)}

            # Create list of dataframes to collate for the barplot drawing
            barplot_df = generate_barplot_df(trials_collated_dfs,
                                             delays(limit_dir, False),
                                             limits)

            # Add 'Link strength' column
            barplot_df.insert(0, 'Link strength', collated_label(limits['moment'],
                                                                 limits['axial']))

            # Add 'Algorithm name' column
            barplot_df.insert(0, 'Algorithm name', algorithm_name.capitalize())

            # Rename delay column appropriately for legend
            barplot_df['Delay (steps)'] = barplot_df['Delay (steps)'].apply(
                element_delay_label)

            barplot_dfs.append(barplot_df)

    barplot_df = pd.concat(barplot_dfs)

    return imported_data, barplot_df


def plot_length_steps(imported_data, algorithm_names, link_strengths, element_delays):
    """Plots the length vs steps graph"""

    # Initialise the figure
    plt.rcParams["font.family"] = "Times New Roman"
    plt.rcParams["font.size"] = "20"
    plt.rcParams["mathtext.fontset"] = "stix"
    figure, axes = plt.subplots(len(link_strengths), len(element_delays), figsize=(1920/96, 1080/96),
                                dpi=96, sharex='col', sharey='row')

    for algorithm_name, colour in zip(algorithm_names, colours_iterator()):
        for limits in limits_in_directory(algorithm_filename(algorithm_name)):

            limit_str = '{}m_{}a'.format(limits['moment'],
                                         limits['axial'])

            # Extract length_steps dataframes
            length_steps_dfs = imported_data[algorithm_name][limit_str]['length_steps']
            delays_list = imported_data[algorithm_name][limit_str]['delays']

            # Add plots to the graph
            row_idx = link_strengths.index(limits)

            for idx, element_delay in enumerate(delays_list):
                col_idx = element_delays.index(element_delay)

                # Average line
                axes[row_idx][col_idx].plot(length_steps_dfs[idx]['Step number'],
                                            length_steps_dfs[idx]['Average length'],
                                            color=colour, label=algorithm_name.capitalize())

                # Error bars
                axes[row_idx][col_idx].fill_between(length_steps_dfs[idx]['Step number'],
                                                    length_steps_dfs[idx]['Min length'],
                                                    length_steps_dfs[idx]['Max length'],
                                                    alpha=0.3, facecolor=colour)

    # Set axis limits
    for idx_strength, ax_row in enumerate(axes):
        limits = link_strengths[idx_strength]
        ax_row[0].set_ylabel(collated_label(limits['moment'], limits['axial'], links=True),
                             fontweight='bold', labelpad=40)
        for idx_delay, ax in enumerate(ax_row):
            ax.set_xlim([0, None])
            ax.set_ylim([0, None])

            if idx_strength == 0:
                ax.set_title('Delay = {}'.format(element_delays[idx_delay]), fontweight='bold')

    axes[-1][-1].legend(loc='lower right')

    # Create large hidden frame to give global titles
    figure.add_subplot(111, frameon=False)
    plt.tick_params(labelcolor='none', top=False,
                    bottom=False, left=False, right=False)
    plt.xlabel('Number of steps taken')
    plt.ylabel('Cantilever length (placed elements)')

    return figure


def define_figure_size(algorithm_names):
    """Searches through the results for the given algorithms and decides how many
    subplots to draw (one for each link strength and element delay)"""

    link_strengths = []
    element_delays = []

    for algorithm_name in algorithm_names:
        for limits in limits_in_directory(algorithm_filename(algorithm_name)):

            # Add limits to link_strengths if not already
            if limits not in link_strengths:
                link_strengths.append(limits)

            limit_dir = limit_directory(algorithm_filename(algorithm_name), limits)

            for element_delay in delays(limit_dir):
                if element_delay not in element_delays:
                    element_delays.append(element_delay)

    return link_strengths, element_delays


def barplot_titles():
    """Returns a list of the dataframe titles and associated filenames of data to draw
    barplots of"""

    return [['Steps taken', 'barplot_steps'],
            ['Length reached', 'barplot_length'],
            ['Timedout elements', 'barplot_timedout'],
            ['Maximum moment criticalness', 'barplot_moment'],
            ['Maximum axial force criticalness', 'barplot_axial']]


def plot_barplots(barplot_df, y_plot, link_strengths):
    """Plots the length against the number of steps taken for different element delays.

    The minimum number of steps for each trial is noted with a triangle, and the average with
    a cross

    Inputs:
        barplot_df      Dataframe containing data in the correct fasion for boxpot drawing
        y_plot          Column title of the data to plot on the y axis
    """

    # Initialise the figure
    plt.rcParams["font.family"] = "Times New Roman"
    plt.rcParams["font.size"] = "20"
    plt.rcParams["mathtext.fontset"] = "stix"
    figure, axes = plt.subplots(len(barplot_df['Link strength'].unique()), 1,
                                figsize=(1920/96, 1080/96),
                                dpi=96)

    x_plot = 'Delay (steps)'
    grouping = 'Algorithm name'

    sns.set_palette(custom_sns_palette())

    for idx, link_strength in enumerate(barplot_df['Link strength'].unique()):
        df_portion = barplot_df.loc[barplot_df['Link strength']
                                    == link_strength]
        sns.barplot(x=x_plot, y=y_plot,
                    data=df_portion,
                    hue=grouping,
                    ax=axes[idx])

        # Set ylabel correctly
        limits = link_strengths[idx]
        axes[idx].set_ylabel(collated_label(limits['moment'], limits['axial'], links=True),
                             fontweight='bold', labelpad=40)

    # Hide titles and legends, and add subplot titles
    for axis in axes:
        if axis != axes[-1]:
            axis.get_legend().remove()
            axis.xaxis.get_label().set_visible(False)

    # Create large hidden frame to give global titles
    figure.add_subplot(111, frameon=False)
    plt.tick_params(labelcolor='none', top=False,
                    bottom=False, left=False, right=False)

    if y_plot == 'Steps taken':
        ylabel = 'Timesteps taken'
    elif y_plot == 'Length reached':
        ylabel = 'Final length (dm)'
    else:
        ylabel = y_plot
    plt.ylabel(ylabel, labelpad=20)

    return figure
