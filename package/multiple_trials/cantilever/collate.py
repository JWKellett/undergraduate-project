"""collate mode"""

import glob
import os
import re

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

from ...basics import collated_label, data_dir, show_or_save
from ...graphics.colours import BLUE, GREEN, ORANGE
from .shared import add_origin, averages, moment_str2int, read_optimal_data


def collate_data(algorithm_name, elements_per_trial, plots):
    """Collates the data into barplots and comparison graphs"""

    # Set directories
    standard_dir = os.path.join(data_dir(), 'Results', algorithm_name)

    # Draw elements_to_length plots
    draw_elements_to_length(standard_dir, elements_per_trial, plots)

    # Draw barplots
    collate_to_barplot(standard_dir, plots)


def draw_elements_to_length(standard_dir, no_elements, plots):
    """Draws a comparison figure for all limits"""

    # Extract directories with results in
    test_dirs = test_directories(standard_dir)

    # Initialise figure with correct number of subplots
    plt.rcParams["font.family"] = "Times New Roman"
    plt.rcParams["font.size"] = "26"
    plt.rcParams["mathtext.fontset"] = "stix"
    fig, axs = plt.subplots(1, len(test_dirs), figsize=(700/96*len(test_dirs), 500/96),
                            dpi=96, sharey=True)

    # Axs needs to be a list
    if not isinstance(axs, np.ndarray):
        axs = [axs]

    # Create one plot per limits set
    for idx, test_dir in enumerate(test_dirs):
        # Extract titles
        moment = int(re.search(r'(\d+)m_', test_dir).group(1))
        axial = int(re.search(r'_(\d+)a', test_dir).group(1))

        limits = {'moment': moment,
                  'axial': axial}

        # Load and plot data and errorbars
        elements_to_length = add_origin(averages(os.path.join(standard_dir, test_dir), limits,
                                                 no_elements)[0])

        axs[idx].plot(elements_to_length['No. elements'], elements_to_length['Average length'],
                      label='Construction algorithm', color=BLUE)

        axs[idx].fill_between(elements_to_length['No. elements'],
                              elements_to_length['5% quartile'],
                              elements_to_length['95% quartile'],
                              alpha=0.2, facecolor=BLUE)

        # Load and plot optimums
        opt_elements_to_length = add_origin(read_optimal_data(limits))
        axs[idx].plot(opt_elements_to_length['No. elements'],
                      opt_elements_to_length['Length'],
                      label='Optimal', color=GREEN)

        # Add titles and limits
        axs[idx].set_title(collated_label(moment, axial, links=True), fontweight='bold')
        axs[idx].set_xlim([0, no_elements])

    # Create large hidden frame to give global xaxis title
    fig.add_subplot(111, frameon=False)
    fig.axes[-1].tick_params(labelcolor='none', top=False,
                             bottom=False, left=False, right=False)
    fig.axes[-1].set_xlabel('Number of agents in structure')
    fig.axes[0].set_ylabel('Maximum stable\ncantilever length (dm)')

    axs[0].set_ylim([0, None])
    axs[-1].legend(loc='lower right')

    # Show or save plot
    show_or_save(fig, plots, os.path.join(standard_dir,
                                          'Collated elements to length'))

    return


def test_directories(standard_dir, comparison_dir=None):
    """Returns a list of results directories in the standard and comparison directories"""

    # Print debug data
    print('Standard directory: ' + standard_dir)

    # List of results directories from standard algorithm
    standard_dirs = []
    for full_path in glob.glob(os.path.join(standard_dir, '*m_*')):
        standard_dirs.append(os.path.split(full_path)[1])

    if comparison_dir is not None:
        # List of results directories from local algorithm
        print('Comparison directory: ' + comparison_dir)
        comparison_dirs = []
        for full_path in glob.glob(os.path.join(comparison_dir, '*m_*')):
            comparison_dirs.append(os.path.split(full_path)[1])

        # Return directories in both sets of results
        common_dirs = list(set(standard_dirs).intersection(comparison_dirs))
        common_dirs.sort(key=moment_str2int)

        return common_dirs

    else:
        # Not comparing
        return standard_dirs


def collate_to_barplot(base_dir, plots):
    """Collates all the maximums data into a single barplot"""

    barplot_df = generate_barplot_df(base_dir)

    # Initialise graph
    fig = plt.figure(figsize=(869/96, 560/96), dpi=96)
    plt.rcParams["font.family"] = "Times New Roman"
    plt.rcParams["font.size"] = "20"
    plt.rcParams["mathtext.fontset"] = "stix"

    sns.barplot(x='Link strength', y='Maximum criticalness', hue='Limit type', ci='sd',
                data=barplot_df, palette=sns.set_palette(sns.color_palette([BLUE, ORANGE])))
    plt.axhline(1, 0, 1, color='k', linestyle='dashed')

    # Show or save
    show_or_save(fig, plots, os.path.join(base_dir, 'barplot'))


def generate_barplot_df(base_dir):
    """Creates the dataframe that is used for plotting the barplot"""

    imported_data = {'Link strength': [],
                     'Maximum criticalness': [],
                     'Limit type': [],
                     'Moment': []}

    for directory in os.listdir(base_dir):
        try:
            moment = int(re.search(r'(\d+)m_', directory).group(1))
            axial = int(re.search(r'_(\d+)a', directory).group(1))

            # Read data
            imported_df = pd.read_csv(os.path.join(
                base_dir, directory, 'maximums.csv'))
        except AttributeError:
            # Not a directory
            continue

        # Extend arrays in imported_data dict
        limits_label = collated_label(moment, axial)
        moment_criticalnesses = (imported_df['moment']/moment).tolist()
        axial_criticalnesses = (imported_df['axial']/axial).tolist()

        imported_data['Link strength'].extend(
            [limits_label]*(len(moment_criticalnesses)+len(axial_criticalnesses)))

        imported_data['Maximum criticalness'].extend(moment_criticalnesses)
        imported_data['Maximum criticalness'].extend(axial_criticalnesses)

        imported_data['Limit type'].extend(['Moment']*len(moment_criticalnesses))
        imported_data['Limit type'].extend(['Axial force']*len(axial_criticalnesses))

        imported_data['Moment'].extend(
            [moment]*(len(moment_criticalnesses)+len(axial_criticalnesses)))

    collated_df = pd.DataFrame(imported_data)
    collated_df.sort_values(by=['Moment'], inplace=True)

    return collated_df
