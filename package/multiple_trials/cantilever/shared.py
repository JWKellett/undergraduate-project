"""Functions shared across different modes"""

import glob
import os
import re

import numpy as np
import pandas as pd

from ...basics import data_dir


def test_directory(algorithm_name, limits):
    """Returns the path of the test directory"""

    algorithm_class = algorithm_name.split('_')[0]

    if algorithm_class == 'sequential':
        # Sequential directory structure
        directory = os.path.join(data_dir(), 'Results', algorithm_name,
                                 child_directory_name(limits))
    elif algorithm_class == 'parallel':
        # Parallel directory structure
        directory = os.path.join(data_dir(), 'Results', algorithm_name,
                                 child_directory_name(limits))

    else:
        raise ValueError("Algorithm class must be 'sequential' or 'parallel'")

    return directory


def child_directory_name(limits):
    """Returns the name of the limits directory"""

    child_dir_str = '{}m_{}a'.format(limits['moment'], limits['axial'])

    return child_dir_str


def comparison_directory(standard_dir, compare_to):
    """Converts the standard directory path to the one we're comparing to"""

    # Comparing to local or extended
    if compare_to in ['local', 'extended', 'simple']:
        precursor = 'distributed_'
        idx = standard_dir.index(precursor)+len(precursor)
        return standard_dir[:idx]+compare_to+'_'+standard_dir[idx:]

    # Comparing versions of the same algorithm
    idx = standard_dir.index('_v')

    if int(standard_dir[idx+2:]) <= int(compare_to[1:]):
        raise ValueError(
            'Standard version number must be higher than comparison version number')
    return standard_dir[:idx+1]+compare_to


def directory_max_version(directory):
    """Returns the highest version directory of the specified directory"""

    # List all versions of the named algorithm in the directory
    versions = sorted(glob.glob(re.sub(r'_v\d+', '_v*', directory)),
                      key=lambda dir: int(re.search(r'_v(\d+)', dir).group(1)))

    return versions[-1]


def trial_str2int(string):
    """Returns integer trial number from string iuncluding 'Trial_xxx.csv' """
    return int(re.search(r'Trial_(\d+).csv', string).group(1))


def moment_str2int(string):
    """Returns integer moment from string iuncluding 'xxxm_' """
    return int(re.search(r'(\d+)m_', string).group(1))


def add_origin(dataframe):
    """Adds 0,0 to the first row in the dataframe"""

    # Error handling if there's no data to append to
    if dataframe is None:
        return None

    headings = list(dataframe)
    newline_dict = {}
    for heading in headings:
        newline_dict[heading] = 0
    newline = pd.DataFrame(newline_dict, index=[0])

    return pd.concat([newline, dataframe])


def read_optimal_data(limits):
    """Reads the optimal data csv"""

    try:
        return pd.read_csv(os.path.join(data_dir(), 'Optimisation', child_directory_name(limits),
                                        'elements_to_length.csv'),
                           names=['No. elements', 'Length'])
    except FileNotFoundError:
        # No optimal data for this limit
        return None


def averages(test_dir, limits, max_elements):
    """Computes the average performance for the specified limits"""

    elements_to_length = pd.DataFrame(
        {'No. elements': range(1, max_elements+1)})

    maximums = {'moment': [], 'axial': []}

    for filename in sorted(glob.glob(os.path.join(test_dir, 'Trial*.csv')), key=trial_str2int):
        trial_data = pd.read_csv(filename)
        trial_no = trial_str2int(filename)

        # Add new column to dataframe with lengths for this trial
        lengths = []
        for idx in range(trial_data.shape[0]-2):

            if idx == trial_data.shape[0]-3:
                diff = max_elements-len(lengths)
            else:
                diff = int(trial_data.at[idx+1, 'No. elements']) - \
                    int(trial_data.at[idx, 'No. elements'])

            lengths.extend([int(trial_data.at[idx, 'Length'])]*diff)

        elements_to_length['Length trial {}'.format(trial_no)] = lengths

        # Also append to maximums dictionary
        maximums['moment'].append(trial_data.iloc[-2, 0])
        maximums['axial'].append(trial_data.iloc[-1, 0])

    # Calculate average length
    elements_to_length['Average length'] = elements_to_length.iloc[:, 1:].mean(axis=1)
    elements_to_length['Max length'] = elements_to_length.iloc[:, 1:].max(axis=1)
    elements_to_length['Min length'] = elements_to_length.iloc[:, 1:].min(axis=1)
    elements_to_length['5% quartile'] = elements_to_length.iloc[:, 1:].quantile(0.05, axis=1)
    elements_to_length['95% quartile'] = elements_to_length.iloc[:, 1:].quantile(0.95, axis=1)

    lengths_df = elements_to_length[['No. elements', 'Average length',
                                     'Max length', 'Min length',
                                     '5% quartile', '95% quartile']]

    avg_moment = np.average(maximums['moment'])
    max_moment = max(maximums['moment'])
    avg_axial = np.average(maximums['axial'])
    max_axial = max(maximums['axial'])
    metadata_df = pd.DataFrame({'Moment (Ndm)': [avg_moment,
                                                 (avg_moment / limits['moment']-1)*100,
                                                 max_moment,
                                                 (max_moment/limits['moment']-1)*100],
                                'Axial (N)': [avg_axial,
                                              (avg_axial / limits['axial']-1)*100,
                                              max_axial,
                                              (max_axial/limits['axial']-1)*100]},
                               index=['Average', 'Avg. percent exception',
                                      'Maximum', 'Max. percent exception'])

    maximums_df = pd.DataFrame(maximums)

    return lengths_df, metadata_df, maximums_df


def limits_in_directory(algorithm_name):
    """Extracts the limit pairs in the given directory"""

    limit_pairs = []
    for directory in glob.glob(os.path.join(data_dir(), 'Results',
                                            algorithm_name, '*m_*a')):
        match_obj = re.search(r'(\d+)m_(\d+)a', directory)
        limit_pairs.append({'moment': int(match_obj.group(1)),
                            'axial': int(match_obj.group(2))})

    # Sort by moment before returning
    return sorted(limit_pairs, key=lambda x: x['moment'])


def limit_directory(algorithm_name, limits):
    """Returns the path to the directory for this limit pair"""

    limit_dir = '{}m_{}a'.format(limits['moment'],
                                 limits['axial']).replace('.', '-')

    return os.path.join(data_dir(), 'Results', algorithm_name, limit_dir)
