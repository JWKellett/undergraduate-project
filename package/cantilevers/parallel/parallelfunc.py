"""Functions for the parallel algorithm"""
import bz2
import os
import pickle as pkl
import re

import numpy as np
import pandas as pd

from ...basics import data_dir

try:
    from ...structures import Cantilever
    from .element import Element
except ImportError:
    # Don't need these imports, it's just for type hinting
    pass


def define_test_directory(limits, element_delay, specials_before, specials_after, trial,
                          random, simple):
    """Returns a string to directory where the test is saved to"""

    test_dir = os.path.join(data_dir(),
                            'auto-generated_structures',
                            'parallel')

    # Random algorithm
    if random:
        test_dir += '_random'

    # Simple algorithm
    if simple:
        test_dir += '_simple'

    # Special characters before
    if specials_before:
        test_dir += '_'+specials_before

    # Allowable limits
    test_dir += '_{}m_{}a'.format(limits['allowable']['moment'],
                                  limits['allowable']['axial'])

    # Element delay
    test_dir += '_{}delay'.format(element_delay)

    # Special characters after
    if specials_after:
        test_dir += '_'+specials_after

    # Temporary directory for images
    image_dir = os.path.join(test_dir, 'TEMP_Trial_{}'.format(trial))

    return test_dir, image_dir


def print_test_info(limits, save, element_delay, test_dir, trial, random, simple):
    """Prints the information about the current test"""
    # Print limits if not saving anything

    if random and not simple:
        extras = 'random'
    elif simple and not random:
        extras = 'simple'
    elif random and simple:
        extras = 'random and simple'
    else:
        extras = ''

    print('Beginning {} parallel test with:'.format(extras))

    print('  Allowable moment = {} Ndm'.format(limits['allowable']['moment']))
    print('  Allowable axial force = {} N'.format(limits['allowable']['axial']))

    if limits['failure']['moment'] is not None:
        print('  Failure moment = {:.0f} Ndm'.format(limits['failure']['moment']))
        print('  Failure axial force = {:.0f} N'.format(limits['failure']['axial']))
    print('  Delay between elements = {}'.format(element_delay))

    if save:
        print('Saving results to {}'.format(test_dir))
        print('  Trial = {}'.format(trial))


def new_active_element(steps_since_last_addition, element_delay, no_elements, max_elements,
                       no_active_elements):
    """Decides when to initialise a new active element"""

    if element_delay > 0:
        # Normally add every set number of elements
        return bool(no_elements < max_elements and steps_since_last_addition >= element_delay)
    else:
        # If delay=0, then we're simulating the non-parallel algorithm
        return bool(no_active_elements == 0)


def export_elements(elements_list, test_dir, trial_no):
    """Exports the list of cantilever.elements objects to a pickled file
    Use bz2 compression to make it much smaller"""

    path = test_dir + '/Trial_{}.pbz2'.format(trial_no)

    with bz2.BZ2File(path, 'w') as file:
        pkl.dump(elements_list, file)


class PlacementOption:
    """Class for the placement options of the new element. Initialises with all fields empty

    Fields:
        column          the column of this option
        type_           Placing in this column due to 'reinforcement' or placing at the 'tip' (or 
                        'random', if in random mode)
        probability     probability of placing in this column
    """

    def __init__(self, col, type_, probability):
        self.column = col
        self.type = type_
        self.probability = probability

    def __repr__(self):
        return "PlacementOption for column {} ({}, prob = {:.3f})".format(self.column,
                                                                          self.type,
                                                                          self.probability)

    def copy(self):
        """Returns a copy of the current placement option"""
        return PlacementOption(self.column, self.type, self.probability)


def remove_placed_elements(active_elements):
    """Removes any elements that are placed (or 'init') from the active_elements array"""

    still_active_elements = []

    for element in active_elements:
        if (not isinstance(element, str)) and element.mode != 'placed':
            still_active_elements.append(element)

    return still_active_elements


def adjacent_locations(location):
    """Returns a list of the adjacent locations (Von Neumann neighbourhood)"""

    adjacents = [[location[0]-1, location[1]],    # North
                 [location[0]+1, location[1]],    # South
                 [location[0], location[1]+1],    # East
                 [location[0], location[1]-1]]    # West

    return adjacents


def moore_neighbourhood_preferences(direction, centre, active_link):
    """Determines the locations that the current element should move to based on
    the direction and current location."""

    # Define offsets (clockwise from north)
    offsets = [[-1, 0],  # N
               [-1, 1],  # NE
               [0, 1],  # E
               [1, 1],  # SE
               [1, 0],  # S
               [1, -1],  # SW
               [0, -1],  # W
               [-1, -1]]  # NW
    labels = ['n', 'ne', 'e', 'se', 's', 'sw', 'w', 'nw']

    # Reverse the list if moving left on the bottom, or right on the top
    if (direction == 'left' and centre[0] >= 0) or (direction == 'right' and centre[0] < 0):
        offsets.reverse()
        labels.reverse()

    # Choose the correct starting point for the lists (start at active_link+1)
    split_at = labels.index(active_link)+1

    # Start at active link if on the side you're moving into
    if (direction == 'right' and active_link in ['n', 'e']) or \
            (direction == 'left' and active_link == 'w'):
        split_at -= 1

    offsets = offsets[split_at:] + offsets[:split_at]

    neighbour_locations = []
    for offset in offsets[:-1]:
        neighbour_locations.append([centre[0] + offset[0],
                                    centre[1] + offset[1]])

    return neighbour_locations


def swap_elements(master: 'Element', slave: 'Element', cantilever: 'Cantilever', limits):
    """Swaps the properties of two elements

    Inputs:
        master      the Element initiating the swap
        slave       the Element being swapped
        cantilever  the Cantilever object containing the master and slave
        limits      the limits dictionary (used for when the slave is gathering)
    Outputs:
        move_again  bool; whether the master element should make another step,
                    as the swap failed
    """

    # Don't swap if the slave is swapping already
    if 'swapping' in slave.mode:
        return False

    # Don't swap if one element is on the top and the other is in row 0
    if set([master.location[0], slave.location[0]]) == set([-1, 0]):
        return False

    print('Attempting to swap elements {} (master) and {} (slave)'.format(master.id, slave.id))

    # If slave is placing, allow it to try to place first if in the correct column
    if slave.mode == 'placing' and slave.location[1] == slave.destination_column:
        slave_can_place = slave.attempt_placement(cantilever, dry_run=True)

        if slave_can_place:
            # Slave can place next turn: master hasn't moved, but it should stay where
            # it is as a penalty
            print('  Slave can place itself next step, so not swapping')
            return False

    # Update the slave's direction in case it hasn't moved yet
    slave.set_direction(cantilever)

    # Resolve conflicts if both elements are on the top
    if (master.location[0] < 0 and slave.location[0] < 0):
        place_again = resolve_top_conflicts(master, slave)

        # Master can try to take another step
        return place_again

    # Resolve conflicts when both elements want to move in the same direction
    if master.direction == slave.direction:

        keep_swapping = resolve_same_direction(master, slave)

        if not keep_swapping:
            # Master hasn't moved yet, so can take another step (if not none)
            return bool(master.override_next_step in ['left', 'right'])

    # Allow slave to gather data if it hasn't yet
    if slave.mode == 'gathering' and not slave.advanced_this_step:
        try:
            external_element = cantilever.elements[slave.location[0]-np.sign(slave.location[0]),
                                                   slave.location[1]]
            if external_element.mode == 'placed':
                # Only read the data if this is a placed element
                slave.traverse_data.append_criticalness_local(external_element, limits,
                                                              slave.location)
        except KeyError:
            # No element at this location
            pass

    # Make copy of the old slave objects
    slave_destination_column = slave.destination_column
    slave_mode = slave.mode
    slave_placement_options = slave.placement_options
    slave_traverse_data = slave.traverse_data
    slave_override_next_step = slave.override_next_step

    # Update slave
    slave.destination_column = master.destination_column
    slave.mode = master.mode
    slave.placement_options = master.placement_options
    slave.traverse_data = master.traverse_data
    slave.override_next_step = master.override_next_step

    # Update master
    master.destination_column = slave_destination_column
    master.mode = slave_mode
    master.placement_options = slave_placement_options
    master.traverse_data = slave_traverse_data
    master.override_next_step = slave_override_next_step

    # Set both elements to swapping mode
    master.swap_mode(cantilever)
    slave.swap_mode(cantilever)

    # Add a flag to the slave to make it wait if it hasn't moved this step yet
    if not slave.advanced_this_step:
        slave.swap_mode(cantilever, True)

    # Replot slave in red in case it's already moved this step
    slave.plot()

    return False


def get_left_right_elements(element1: 'Element', element2: 'Element'):
    """Determines which element is on the left and right"""

    if element1.location[1] > element2.location[1]:
        right_element = element1
        left_element = element2
    elif element1.location[1] < element2.location[1]:
        right_element = element2
        left_element = element1
    elif element1.location[0] < element2.location[0]:
        right_element = element1
        left_element = element2
    else:
        right_element = element2
        left_element = element1

    return left_element, right_element


def diagonal_locations(location1, location2):
    """Determines if two locations are directly attached, or diagonally related"""

    # Calculate difference in row and column locations
    row_diff = abs(location1[0] - location2[0])
    col_diff = abs(location1[1] - location2[1])

    # Diagonal means row and col are different
    return bool(row_diff != 0 and col_diff != 0)


def resolve_top_conflicts(master: 'Element', slave: 'Element'):
    """Resolves conflicts when both elements are on the top of the cantilever

    Left element will step left, and the right element will either stay where it is (if above a
    placed element) or step left (if above an active element)
    """

    # Determine which element is on the left and right
    left_element, right_element = get_left_right_elements(master, slave)

    if right_element.direction == 'right':
        # Both elements heading right, so it's simply the case that the left element
        # is trying to move first. Don't do anything and right element will move
        print('  Left element trying to move first')
        return False

    # Right element is heading left, so the left element needs to head left too
    left_element.override_next_step = 'left'
    print('  Left element must head left')

    # Try again if left element is the master (should be never)
    return bool(left_element == master)


def resolve_same_direction(master: 'Element', slave: 'Element'):
    """When two elements are trying to swap but want to move in the same direction, it can be tricky
    to resolve.

    Outputs:
        keep_swapping    bool; whether to continue with the swap
    """

    # Determine which element is on the left and right
    left_element, right_element = get_left_right_elements(master, slave)

    # Store movement direction as a separate variable
    direction = left_element.direction

    # Leftmost element has passive link and they're trying to move left; return
    # This situation only occurs when the map is as below:
    # 11111
    # 111
    #   22
    if direction == 'left' and left_element.passive_links:
        right_element.override_next_step = 'right'
        return False

    # Rightmost element has passive link and they're trying to move right; return
    # This situation only occurs when the map is as below:
    # 11111
    # 111
    # 1112
    #    2
    if direction == 'right' and right_element.passive_links:
        left_element.override_next_step = 'left'
        return False

    # Rear remains still

    # If heading right, element on the left stays still
    if direction == 'right':
        print('  Overriding left element (id {}) to stay still'.format(left_element.id))
        left_element.override_next_step = 'none'
        return False

    # If heading left, element on the right stays still
    if direction == 'left':
        print('  Overriding right element (id {}) to stay still'.format(right_element.id))
        right_element.override_next_step = 'none'
        return False

    raise Exception('Should never reach this point')


def multiple_trials_row(step_no, cantilever, complete_link_internals, elements_timedout):
    """Creates a list which will become the row of the multiple trials export.

    The format is:
    | Step no. |   Length of     |            No. elements            | Maximum |
    |          | placed elements | Total | Placed | Active | Timedout |  M  | F |
    """

    length = sum(cantilever.map[cantilever.row_offset] == 1)
    elements_total = cantilever.no_elements
    elements_placed = sum(sum(cantilever.map == 1))
    elements_active = elements_total - elements_placed

    maximum_m = np.max(complete_link_internals['moment'])
    maximum_f = np.max(complete_link_internals['axial'])

    return [step_no, length, elements_total, elements_placed, elements_active,
            elements_timedout, maximum_m, maximum_f]


def multiple_trials_dir(test_dir):
    """Converts the standard test directory string to the one for multiple trials"""

    test_str = os.path.split(test_dir)[1]

    delay_str = re.search(r'(\d+delay)', test_str)[1]

    if delay_str[0] == '0':
        # If delay = 0, save to sequential directory
        match_obj = match_obj = re.search(r'(.+)_(\d+m_\d+a)', test_str)
        alg_ver_str = match_obj[1]
        limit_str = match_obj[2]

        export_dir = os.path.join(data_dir(), 'Results',
                                  alg_ver_str+'_delay0', limit_str)
    else:

        # Separate directories for algorithm version and limit pair
        match_obj = re.search(r'(.+)_(\d+m_\d+a)_.+delay', test_str)
        alg_ver_str = match_obj[1]
        limit_str = match_obj[2]

        export_dir = os.path.join(data_dir(), 'Results',
                                  alg_ver_str, limit_str, delay_str)

    return export_dir


def multiple_trials_csv(multiple_trials_export_list, export_dir, trial_no):
    """Exports the list of data to export to a csv file"""

    # Convert to dataframe
    multiple_trials_export_df = pd.DataFrame(multiple_trials_export_list,
                                             columns=['Step no',
                                                      'Length of placed elements',
                                                      'No. elements (total)',
                                                      'No. elements (placed)',
                                                      'No. elements (active)',
                                                      'No. elements (timed out)',
                                                      'Maximum moment (Ndm)',
                                                      'Maximum axial force (N)'])

    # Define path
    path = os.path.join(export_dir, 'Trial_{}.csv'.format(trial_no))

    # Export to csv
    multiple_trials_export_df.to_csv(path, index=False,
                                     float_format='%.3f')
