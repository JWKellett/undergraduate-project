"""Collate mode"""

import glob
import os
import re

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from ...basics import collated_label, data_dir, show_or_save
from ...graphics.colours import BLUE, CYAN, GREEN, ORANGE, PURPLE, RED, YELLOW


def collate_results(fonts, font_size, draw_mode):
    """Collates the results in the results directory"""

    # Iterate through all directories
    directories = glob.glob(os.path.join(data_dir(),
                                         'Optimisation/*m_*a*'))

    # Initialise dictionary with xy data in it
    collated_dict = {}

    for directory in directories:
        elements_to_length = create_elements_to_length(directory)

        # List from maximum length to number of elements
        length_to_elements = pd.DataFrame(np.flip(elements_to_length[np.unique(elements_to_length[:, 1],
                                                                               return_index=True)[1], :],
                                                  1), columns=['Length', 'Min no. elements'])

        # Add valid configurations to length_to_elements
        length_to_elements['Configuration(s)'] = length_to_elements.apply(
            lambda row: extract_configurations(row, directory), axis=1)

        # Export .csv files
        np.savetxt(os.path.join(directory, 'elements_to_length.csv'), elements_to_length,
                   delimiter=',', fmt='%d')
        length_to_elements.to_csv(os.path.join(directory, 'length_to_elements.csv'), index=False)

        # Add the length_to_elements data to the xy data dictionary
        add_to_xy_dict(collated_dict, directory, elements_to_length)

    # Draw the collated graph
    fig = collated_graph(collated_dict, fonts, font_size)
    show_or_save(fig, draw_mode, os.path.join(data_dir(), 'Optimisation', 'Collated_data'))


def create_elements_to_length(directory):
    """Creates the elements_to_length array"""
    filenames = sorted(glob.glob(os.path.join(directory, '*elements_*l.csv')),
                       key=lambda x: int(re.search(r'(\d+)elements', x).group(1)))

    # List from number of elements to maximum length
    elements_to_length = np.zeros((int(re.search(r'(\d+)elements', filenames[-1]).group(1)), 2),
                                  dtype=int)
    for filename in filenames:
        no_elements = int(re.search(r'(\d+)elements', filename).group(1))
        length = int(re.search(r'(\d+)l.csv', filename).group(1))

        elements_to_length[no_elements-1, 0] = no_elements
        elements_to_length[no_elements-1, 1] = length

    # Add any rows that don't have their own csv file (arising from optimums_large)
    for idx, row in enumerate(elements_to_length):
        if np.equal(row, np.array([0, 0])).all():
            row[0] = elements_to_length[idx-1][0]+1
            row[1] = elements_to_length[idx-1][1]

    # Add rows that have been tested and are known to be unstable
    try:
        unstable_elements = int(
            re.search(r'(\d+)elements.+_UNSTABLE',
                      glob.glob(os.path.join(directory, '*elements_*l_UNSTABLE.csv'))[0]).group(1))

        unstable_rows = np.vstack(
            (np.arange(elements_to_length[-1, 0]+1, unstable_elements+1),
             elements_to_length[-1, 1]*np.ones(unstable_elements-elements_to_length[-1, 0],
                                               dtype=int))).T

        return np.vstack((elements_to_length, unstable_rows))
    except IndexError:
        # All entries are stable
        return elements_to_length


def extract_configurations(row, directory):
    """Extracts the configuration(s) that get the longest for the min elements"""

    # Open file as dataframe
    filename = os.path.join(directory,
                            '{}elements_{}l.csv'.format(row['Min no. elements'], row['Length']))
    file_contents = pd.read_csv(filename)

    # Get configurations
    configurations = file_contents['Configuration'].tolist()
    return str(configurations).strip('[]')


def add_to_xy_dict(xy_dict, directory, elements_to_length):
    """Adds the elements_to_length array to the correct entry in the dictionary"""

    # Extract moment and axial force from directory
    moment = int(re.search(r'(\d+)m_', directory).group(1))
    axial = int(re.search(r'm_(\d+)a', directory).group(1))

    # Add 0,0 to the start
    elements_to_length = np.vstack((np.zeros(2), elements_to_length))

    # Find correct part of dictionary
    if moment in xy_dict:
        # Moment already added
        xy_dict[moment][axial] = elements_to_length
    else:
        # Moment not already added, so a new entry
        xy_dict[moment] = {axial: elements_to_length}


def collated_graph(collated_dict, fonts, font_size):
    """Draws a graph comparing the optimum lengths"""

    # Initialise graph
    fig = plt.figure(figsize=(869/96, 560/96), dpi=96)

    plt.rcParams["font.size"] = font_size
    if fonts == 'tnr':
        plt.rcParams["font.family"] = "Times New Roman"
        plt.rcParams["mathtext.fontset"] = "stix"
    elif fonts == 'latex':
        plt.rcParams["font.family"] = "serif"
        plt.rcParams["font.serif"] = "cmr10"
        plt.rcParams["mathtext.fontset"] = "cm"
        plt.rcParams['axes.unicode_minus'] = False
    else:
        raise ValueError("fonts must be 'latex' or 'tnr'")

    max_length = 0
    max_elements = 0

    # Loop through dictionary
    for moment in sorted(collated_dict.keys()):
        for axial in sorted(collated_dict[moment].keys()):
            label = collated_label(moment, axial)

            # Update the maximums used for axis limits
            max_elements = max(max(collated_dict[moment][axial][:, 0]), max_elements)
            max_length = max(max(collated_dict[moment][axial][:, 1]), max_length)

            # Get colour and linestyle
            colour = line_format(moment, collated_dict.keys())

            plt.plot(collated_dict[moment][axial][:, 0],
                     collated_dict[moment][axial][:, 1],
                     label=label, color=colour)

    # Round axes to nearest specified multiple
    m = 5
    xmax = int(m*np.ceil(max_elements/m))
    ymax = int(m*np.ceil(max_length/m))

    # Titles
    plt.xlabel('Number of elements in cantilever')
    plt.ylabel('Maximum stable cantilever length (dm)')
    plt.axis([0, xmax, 0, ymax])

    # Ticks
    plt.xticks(list(range(0, xmax+m, m*2)))
    plt.yticks(list(range(0, ymax+m, m)))

    plt.legend(loc='upper left', frameon=False, prop={'size': font_size*0.7})

    # Hide box
    plt.gca().spines['right'].set_visible(False)
    plt.gca().spines['top'].set_visible(False)

    return fig


def line_format(moment, moments):
    """Sets the line format depending on the ranking of the moments and axials"""

    # Order lists
    moments = sorted(moments)

    # Pick colour based on moment
    idx_moment = moments.index(moment)
    if idx_moment == 0:
        colour = BLUE
    elif idx_moment == 1:
        colour = ORANGE
    elif idx_moment == 2:
        colour = YELLOW
    elif idx_moment == 3:
        colour = PURPLE
    elif idx_moment == 4:
        colour = GREEN
    elif idx_moment == 5:
        colour = CYAN
    elif idx_moment == 6:
        colour = RED
    else:
        raise ValueError('Not enough colours!')

    return colour
